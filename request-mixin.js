Vue.mixin({
    data() {
        return {
            message:null,
            success:null,
            errors:null,
            loading:false,
        }
    },
    methods: {
        setLoading(status) {
            this.$set(this.$data, 'loading', status);
        },

        fillForm(data, target) {
            if (data && target) {
                _.each(target, (val, key) => {
                    if (data[key]) {
                        this.$set(target, key, data[key])
                    }
                })
            }
        },

        findError(key, replace) {
            if (this.errors) {
                let err = this.errors;

                if (err[key]) {
                    if (replace) {
                        if (typeof replace === 'string') {
                            return err[key][0].replace(key, replace);
                        }
                        return err[key][0].replace(replace[0], replace[1]);
                    }
                    return err[key][0];
                }
            }
            return null;
        },

        clearForm() {
            if (this.success) {

                this.$set(this.$data, 'errors', null);

                _.each(this.form, (value, index) => {
                    this.$set(this.form, index, null);
                });
            }
        },

        getRequest(url) {

            this.setLoading(true);

            return axios.get(url).then(response => {
                this.setLoading(false);
                return response.data;
            }).catch(error => {
                this.setLoading(false);
                console.log(error);
                return error.response.data;
            });
        },

        postRequest(url, formData, clearForm) {

            this.handlePreSubmit();

            return axios.post(url, formData).then(response => {
                this.setLoading(false);
                let data = response.data;

                this.$set(this.$data, 'message', data.message);
                this.$set(this.$data, 'success', data.pass || true);

                if (clearForm) this.clearForm();

                return data;
            }).catch((error) => {
                return this.handleRejection(error)
            });
        },

        putRequest(url, formData, clearForm) {

            this.handlePreSubmit();

            return axios.put(url, formData).then(response => {
                this.setLoading(false);
                let data = response.data;

                this.$set(this.$data, 'message', data.message);
                this.$set(this.$data, 'success', data.pass || true);

                if (clearForm) this.clearForm();

                return data;
            }).catch((error) => {
                return this.handleRejection(error)
            });
        },

        handlePreSubmit() {
            this.setLoading(true);
            this.$set(this.$data, 'message', null);
            this.$set(this.$data, 'success', null);
            this.$set(this.$data, 'errors', null);
        },

        handleRejection(error) {
            this.setLoading(false);
            let data = error.response.data;

            console.log(data);
            if (data.message) this.$set(this.$data, 'message', data.message);
            if (data.errors) this.$set(this.$data, 'errors', data.errors);

            this.$set(this.$data, 'success', data.pass || false);

            return data;
        }
    }
});

